//
//  track.h
//  
//
//  Created by Jonathan Noyola on 4/24/14.
//
//

#ifndef _track_h
#define _track_h

#include <pthread.h>

#include "tracer.h"

typedef struct TrackPiece {
    float length;
    float angle;
    int swap;
    
    float shouldSwap;
    
    int alreadySwapped;
} TrackPiece;

typedef struct Lane {
    float offset;
} Lane;

typedef struct SimTick {
    float throttle;
    float speed;
    float angle;
    float angleSpeed;
    int pieceIndex;
    float inPieceDistance;
    struct SimTick *prev;
    struct SimTick *next;
} SimTick;

typedef struct Track {
    int numPieces;
    TrackPiece *pieces;
    
    int numLanes;
    Lane *lanes;
    
    float kEnginePower;
    float kResistance;
    float kBreakoutAngle;
    float kA;
    float kB;
    float kD;
    float kM;
    
    int isQuals;
    int laps;
    float speedForTurboPos;
    int turboTime;
    int turboDuration;
    float turboFactor;
    int isInitialized;
    
    pthread_t simThread;
    int startPieceIndex;
    SimTick *simTickStart;
    int doneSimulating;
} Track;

// Constructor
void track_new(Track *this, int numPieces, int numLanes);

// Destructor
void track_delete(Track *this);

// Pieces
void track_initPiece(Track *this, int index);
float track_pieceLength(Track *this, int index, int lane);
int track_normalizePieceIndex(Track *this, int index);

// Analysis
void track_logTick(Track *this, Tracer *pTracer);
int track_isDoneLogging(Track *this);
float track_predictSpeed(Track *this, float speed, float throttle, float dt);
float track_getDesiredThrottle(Track *this, Tracer *pTracer, float speed, float desiredSpeed, float dt);
float track_getIdealThrottle(Track *this, Tracer *pTracer, float distance);
int track_isStraight(Track *this, int index, int length);
float track_getNextAngle(Track *this, Tracer *pTracer);
void *track_simulate(void *args);
void track_setThrottleForSimTick(Track *this, SimTick *simTick, int laneIndex);
float track_simAngleForThrottle(Track *this, SimTick *simTick, float throttle, int laneIndex);
float track_simNextAngle(Track *this, int pieceIndex, int laneIndex, float speed, float angle, float angleSpeed);
float track_simNextAngleAcc(Track *this, int pieceIndex, int laneIndex, float speed, float angle, float angleSpeed);

#endif
