#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "cJSON.h"
#include "track.h"
#include "tracer.h"

#define NAME_LENGTH 10
#define TRACK_NAME "usa"
#define CAR_COUNT 1
#define SWAP_TICK 5
#define FORCE_QUALS 0
#define QUALS_FINAL_LAP_PICKUP_LENGTH 15
#define MIN_TURBO_LENGTH 4
#define MIN_FULL_THROTTLE_LENGTH 1

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *joinRace_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle, int tick);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *make_simple_msg(char *type, char *data, int tick);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void yourCar(cJSON *msg);
static void gameInit(cJSON *msg, Track *pTrack);
static void carPositions(int sock, cJSON *msg, char *myName, Track *pTrack, Tracer *pTracer);
static cJSON *getStandardMsg(Track *pTrack, Tracer *pTracer, float multiplier, int canTurbo, int tick);
static void turboAvailable(cJSON *msg, Track *pTrack, Tracer *pTracer);
static void turboStart(cJSON *msg, Tracer *pTracer, char *myName);
static void turboEnd(cJSON *msg, Tracer *pTracer, char *myName);
static void crash(cJSON *msg, Track *pTrack, Tracer *pTracer, char *myName);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");
    
    int flag = 1;
    if (setsockopt(fd, IPPROTO_TCP, 1, &flag, sizeof(int)) < 0) // TCP_NODELAY
        printf("Cannot set TCP_NODELAY option "
               "on listen socket (%s)\n", strerror(errno));

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(int sock, char *msg_type_name, cJSON *msg, char *name, Track *pTrack, Tracer *pTracer)
{
    if (!strcmp("yourCar", msg_type_name)) {
        yourCar(msg);
    } else if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameInit", msg_type_name)) {
        gameInit(msg, pTrack);
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
        msg = throttle_msg(1.0, 0);
        pTracer->throttle = 1.0;
        write_msg(sock, msg);
    } else if (!strcmp("carPositions", msg_type_name)) {
        carPositions(sock, msg, name, pTrack, pTracer);
    } else if (!strcmp("turboAvailable", msg_type_name)) {
        turboAvailable(msg, pTrack, pTracer);
    } else if (!strcmp("turboStart", msg_type_name)) {
        turboStart(msg, pTracer, name);
    } else if (!strcmp("turboEnd", msg_type_name)) {
        turboEnd(msg, pTracer, name);
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
        crash(msg, pTrack, pTracer, name);
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        cJSON *msg_data;
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    srand(time(NULL));
    char name[NAME_LENGTH + 1];
    for (int i = 0; i < strlen(argv[3]); i++) {
        name[i] = argv[3][i];
    }
    for (int i = strlen(argv[3]); i < NAME_LENGTH; i++) {
        name[i] = '0' + (rand() % 10);
    }
    name[NAME_LENGTH] = '\0';
    
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
    //json = joinRace_msg(name, argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);
    
    Track track;
    Tracer tracer;
    tracer_new(&tracer);
    
    //FILE *fp = fopen("hwo_out.txt", "w");
    float lastAngle = 0;
    float lastAngleSpeed = 0;
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        
        
        
        float nextAngle = 0;//track_getNextAngle(&track, &tracer);
        
        // If the message was a "carPositions" message, then log_message should
        // elicit a response
        log_message(sock, msg_type_name, json, name, &track, &tracer);
        
        /*float angleSpeed = tracer.angle - lastAngle;
        float angleAcc = angleSpeed - lastAngleSpeed;
        
        float nextAngle = tracer.angle;
        float radius = 0;
        float RHS = 0;
        float kA = 0.099314500;
        float kB = 0.011805708;
        
        if (tracer.tick > 1) {
            float m = 4.2683431;
            float b = -1.3875548;
            float dt = 1;
            
            int index = tracer.pieceIndex;
            float dist = tracer.inPieceDistance;
            float v = tracer.speed;
            float lastAngle = tracer.lastAngle;
            for (int i = 0; i < 20; i++) {
                float length = track_pieceLength(&track, index, tracer.startLaneIndex);
                if (dist > length) {
                    index++;
                    dist -= length;
                }
                if (track.pieces[index].angle == 0) {
                    RHS = 0;
                }
                else {
                    radius = track.pieces[index].length - track.lanes[tracer.startLaneIndex].offset;
                    v = track_predictSpeed(&track, v, track_getDesiredThrottle(&track, v, 8.5, dt), dt);
                    RHS = v * v / radius;
                }
                RHS = m * RHS + b; // = m * max(RHS, x) + b
                RHS = RHS > 0 ? RHS : 0;
                float currAngle = nextAngle;
                nextAngle = (RHS * dt + (2 + kA) * currAngle - lastAngle) / (1 + kA + kB * dt);
                lastAngle = currAngle;
                dist += v;
            }
        }
        float LHS = angleAcc + kA*angleSpeed + kB*tracer.angle;
         */
        
        //printf("throttle: %f   speed: %f   angle: %f   predicted: %f   piece: %d   %d\n", tracer.throttle, tracer.speed, tracer.angle, nextAngle, tracer.pieceIndex, tracer.tick);
        if (tracer.pieceIndex >= 8 && tracer.pieceIndex <= 11) {
            //float dist = (tracer.pieceIndex - 8) * 70.69 + tracer.inPieceDistance;
            //fprintf(fp, "%f %f\n", lastRHS, LHS);
        } else if (tracer.pieceIndex >= 18 && tracer.pieceIndex <= 21) {
            //float LHS = angleAcc + 0.0993355*angleSpeed + 0.0118059*tracer.angle;
            //float RHS = tracer.speed * tracer.speed / 180;
            //float dist = (tracer.pieceIndex - 18) * 70.69 + tracer.inPieceDistance;
            //fprintf(fp, "%f %f\n", RHS, LHS);
        } else if (tracer.pieceIndex >= 12 && tracer.pieceIndex <= 17) {
            //fprintf(fp, "%f %f %f\n", tracer.angle, angleSpeed, angleAcc);
        }
        /*
        lastAngle = tracer.angle;
        lastAngleSpeed = angleSpeed;
         */

        cJSON_Delete(json);
    }
    
    track_delete(&track);
    //fclose(fp);

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *joinRace_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", botId);
    cJSON_AddStringToObject(data, "trackName", TRACK_NAME);
    cJSON_AddStringToObject(data, "password", "pass456");
    cJSON_AddItemToObject(data, "carCount", cJSON_CreateNumber(CAR_COUNT));
    
    return make_msg("joinRace", data);
}

static cJSON *throttle_msg(double throttle, int tick)
{
    cJSON *json = make_msg("throttle", cJSON_CreateNumber(throttle));
    cJSON_AddItemToObject(json, "gameTick", cJSON_CreateNumber(tick));
    return json;
    
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *make_simple_msg(char *type, char *data, int tick)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddStringToObject(json, "data", data);
    cJSON_AddItemToObject(json, "gameTick", cJSON_CreateNumber(tick));
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}

static void yourCar(cJSON *msg)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        cJSON *name = cJSON_GetObjectItem(msg_data, "name");
        cJSON *color = cJSON_GetObjectItem(msg_data, "color");
        printf("%s %s\n", name->valuestring, color->valuestring);
    }
}

static void gameInit(cJSON *msg, Track *pTrack)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        // Fetch track data
        cJSON *race = cJSON_GetObjectItem(msg_data, "race");
        cJSON *track = cJSON_GetObjectItem(race, "track");
        cJSON *pieces = cJSON_GetObjectItem(track, "pieces");
        cJSON *lanes = cJSON_GetObjectItem(track, "lanes");
        int numPieces = cJSON_GetArraySize(pieces);
        int numLanes = cJSON_GetArraySize(lanes);
        
        // Check if this is quals
        cJSON *raceSession = cJSON_GetObjectItem(race, "raceSession");
        cJSON *laps = cJSON_GetObjectItem(raceSession, "laps");
        int numLaps = 0;
        if (laps == NULL) {
            numLaps = -1;
        } else if (pTrack->isInitialized == 1)
            pTrack->laps = laps->valueint;
        
        if (pTrack->isInitialized == 1) {
            pTrack->isQuals = 0;
            return;
        }
        
        // Create new track
        track_new(pTrack, numPieces, numLanes);
        
        if (numLaps == -1) {
            pTrack->laps = 3;
            pTrack->isQuals = 1;
        } else
            pTrack->laps = numLaps;
        
        // Input pieces
        for (int i = 0; i < numPieces; i++) {
            cJSON *piece = cJSON_GetArrayItem(pieces, i);
            track_initPiece(pTrack, i);
            for (int j = 0; j < cJSON_GetArraySize(piece); j++) {
                cJSON *item = cJSON_GetArrayItem(piece, j);
                if (!strcmp("length", item->string)) {
                    pTrack->pieces[i].length = item->valuedouble;
                } else if (!strcmp("radius", item->string)) {
                    pTrack->pieces[i].length = item->valuedouble;
                } else if (!strcmp("angle", item->string)) {
                    pTrack->pieces[i].angle = item->valuedouble;
                } else if (!strcmp("switch", item->string)) {
                    pTrack->pieces[i].swap = item->type;
                }
            }
        }
        
        // Input lanes
        for (int i = 0; i < numLanes; i++) {
            cJSON *lane = cJSON_GetArrayItem(lanes, i);
            int index = cJSON_GetObjectItem(lane, "index")->valueint;
            int distanceFromCenter = cJSON_GetObjectItem(lane, "distanceFromCenter")->valueint;
            pTrack->lanes[index].offset = distanceFromCenter;
        }
        
        // Analyze data
        int firstSwapIndex = -1;
        for (int i = 0; i < pTrack->numPieces; i++) {
            if (pTrack->pieces[i].swap == 1) {
                firstSwapIndex = i;
                break;
            }
        }
        if (firstSwapIndex != -1) {
            int swapIndex = firstSwapIndex;
            float totalAngle = 0;
            for (int i = firstSwapIndex; i < pTrack->numPieces + firstSwapIndex; i++) {
                int index = track_normalizePieceIndex(pTrack, i);
                if (pTrack->pieces[index].swap == 1) {
                    pTrack->pieces[swapIndex].shouldSwap = totalAngle;
                    swapIndex = index;
                    totalAngle = 0;
                }
                totalAngle += pTrack->pieces[index].angle;
            }
            pTrack->pieces[swapIndex].shouldSwap = totalAngle;
        }
        
        for (int i = 0; i < pTrack->numPieces; i++) {
            printf("%f   %f   %d %f\n", pTrack->pieces[i].length, pTrack->pieces[i].angle, pTrack->pieces[i].swap, pTrack->pieces[i].shouldSwap);
        }
    }
}

static void carPositions(int sock, cJSON *msg, char *myName, Track *pTrack, Tracer *pTracer)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        int numCars = cJSON_GetArraySize(msg_data);
        for (int i = 0; i < numCars; i++) {
            cJSON *car = cJSON_GetArrayItem(msg_data, i);
            cJSON *id = cJSON_GetObjectItem(car, "id");
            cJSON *name = cJSON_GetObjectItem(id, "name");
            if (!strcmp(myName, name->valuestring)) {
                // my car
                cJSON *tickObj = cJSON_GetObjectItem(msg, "gameTick");
                if (!tickObj)
                    return;
                int tick = tickObj->valueint;
                float angle = cJSON_GetObjectItem(car, "angle")->valuedouble;
                cJSON *piecePosition = cJSON_GetObjectItem(car, "piecePosition");
                int pieceIndex = cJSON_GetObjectItem(piecePosition, "pieceIndex")->valueint;
                float inPieceDistance = cJSON_GetObjectItem(piecePosition, "inPieceDistance")->valuedouble;
                cJSON *lane = cJSON_GetObjectItem(piecePosition, "lane");
                int startLaneIndex = cJSON_GetObjectItem(lane, "startLaneIndex")->valueint;
                int endLaneIndex = cJSON_GetObjectItem(lane, "endLaneIndex")->valueint;
                int lap = cJSON_GetObjectItem(piecePosition, "lap")->valueint;
                
                tracer_update(pTracer, pTrack, pieceIndex, inPieceDistance, startLaneIndex, endLaneIndex, angle, lap, tick);
                track_logTick(pTrack, pTracer);
                
                cJSON *msg = NULL;
                if (pTracer->inPieceCount == SWAP_TICK) {
                    // We need to send the swap message in advance, so check next piece
                    int nextIndex = track_normalizePieceIndex(pTrack, pieceIndex + 1);
                    if (pTrack->pieces[nextIndex].shouldSwap != 0.0 && !pTrack->pieces[nextIndex].alreadySwapped) {
                        char *direction = pTrack->pieces[nextIndex].shouldSwap < 0.0 ? "Left" : "Right";
                        msg = make_simple_msg("switchLane", direction, tick);
                        printf("Lane Switch: %s\n", direction);
                        pTrack->pieces[nextIndex].alreadySwapped = 1;
                    }
                }
                float throttle = 0.0;
                int index = 0;
                if (msg == NULL) {
                    if (pTrack->isQuals || FORCE_QUALS) {
                        switch (pTracer->lap) {
                            case 0:
                                // go full throttle until crash to get all constants
                                if (!track_isDoneLogging(pTrack)) {
                                    if (pTrack->pieces[pTracer->pieceIndex].angle != 0) {
                                        if (angle == 0) {
                                            throttle = track_getDesiredThrottle(pTrack, pTracer, pTracer->speed, pTracer->speed + 0.1, 1);
                                        } else {
                                            throttle = track_getDesiredThrottle(pTrack, pTracer, pTracer->speed, pTracer->speed - 0.1, 1);
                                        }
                                    } else {
                                        throttle = 1.0;
                                    }
                                } else if (pTrack->kBreakoutAngle == -1) {
                                    throttle = 1.0;
                                } else {
                                    // then go slowly
                                    //msg = getStandardMsg(pTrack, pTracer, 0.5, 0, tick);
                                    msg = getStandardMsg(pTrack, pTracer, 1, 0, tick);
                                }
                                break;
                                
                            /*case 1:
                                if (pTracer->pieceIndex < pTrack->numPieces / 2)
                                    msg = getStandardMsg(pTrack, pTracer, 0.5, 0, tick);
                                else
                                    msg = getStandardMsg(pTrack, pTracer, 1, 0, tick);
                                break;*/
                                
                            default:
                                msg = getStandardMsg(pTrack, pTracer, 1, 1, tick);
                                break;
                        }
                    } else {
                        msg = getStandardMsg(pTrack, pTracer, 1, 1, tick);
                    }
                }
                
                if (msg == NULL) {
                    msg = throttle_msg(throttle, tick);
                    pTracer->throttle = throttle;
                }
                write_msg(sock, msg);
                cJSON_Delete(msg);
            }
        }
    }
}

static cJSON *getStandardMsg(Track *pTrack, Tracer *pTracer, float multiplier, int canTurbo, int tick)
{
    cJSON *msg = NULL;
    if (pTracer->turboAvailable == 1 && canTurbo) {
        if (track_isStraight(pTrack, pTracer->pieceIndex, MIN_TURBO_LENGTH)
            || (pTracer->lap == pTrack->laps - 1 && track_isStraight(pTrack, pTracer->pieceIndex, pTrack->numPieces - pTracer->pieceIndex))) {
            msg = make_simple_msg("turbo", "ZOOOOOOM!", tick);
            pTracer->turboAvailable = 0;
            pTracer->firstTurnAfterTurbo = 1;
            printf("Turbo!\n");
        }
    }
    if (msg == NULL) {
        float throttle;
        if (pTracer->lap == pTrack->laps - 1 && track_isStraight(pTrack, pTracer->pieceIndex, pTrack->numPieces - pTracer->pieceIndex)) {
            throttle = 1.0;
        } else if (pTracer->firstTurnAfterTurbo && pTrack->pieces[pTracer->pieceIndex].angle != 0 && pTracer->speed > 9) {
            throttle = 0;
        } else if (track_isStraight(pTrack, pTracer->pieceIndex, MIN_TURBO_LENGTH)) {
            throttle = 1.0;
        } else if (pTracer->speed < 10 && track_isStraight(pTrack, pTracer->pieceIndex, MIN_FULL_THROTTLE_LENGTH)) {
            throttle = 1.0;
        } else {
            throttle = track_getIdealThrottle(pTrack, pTracer, 10);
        }
        throttle *= multiplier;
        msg = throttle_msg(throttle, tick);
        pTracer->throttle = throttle;
    }
    return msg;
}

static void turboAvailable(cJSON *msg, Track *pTrack, Tracer *pTracer)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        int gameTick = cJSON_GetObjectItem(msg, "gameTick")->valueint;
        int turboDurationTicks = cJSON_GetObjectItem(msg_data, "turboDurationTicks")->valueint;
        float turboFactor = cJSON_GetObjectItem(msg_data, "turboFactor")->valuedouble;
        
        pTracer->turboAvailable = 3;
        pTracer->turboFactor = turboFactor;
        
        if (pTrack->turboTime == -1) {
            pTrack->turboTime = gameTick;
            pTrack->turboDuration = turboDurationTicks;
            pTrack->turboFactor = turboFactor;
        }
        printf("::: AVAILABLE :::\n");
    }
}

static void turboStart(cJSON *msg, Tracer *pTracer, char *myName)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        cJSON *name = cJSON_GetObjectItem(msg_data, "name");
        if (!strcmp(myName, name->valuestring)) {
            pTracer->turbo = 1;
        }
    }
}

static void turboEnd(cJSON *msg, Tracer *pTracer, char *myName)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        cJSON *name = cJSON_GetObjectItem(msg_data, "name");
        if (!strcmp(myName, name->valuestring)) {
            pTracer->turbo = 0;
        }
    }
}

static void crash(cJSON *msg, Track *pTrack, Tracer *pTracer, char *myName)
{
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
        puts("Unknown error");
    else {
        cJSON *name = cJSON_GetObjectItem(msg_data, "name");
        if (!strcmp(myName, name->valuestring)) {
            if (pTrack->kBreakoutAngle == -1)
                pTrack->kBreakoutAngle = pTracer->angle - 5;
            tracer_crash(pTracer);
        }
    }
}
