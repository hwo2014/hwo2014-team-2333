//
//  Tracer.c
//  
//
//  Created by Jonathan Noyola on 4/24/14.
//
//

#include <stdio.h>

#include "tracer.h"
#include "track.h"

void tracer_new(Tracer *this)
{
    this->pieceIndex = 0;
    this->inPieceDistance = 0;
    this->inPieceCount = 0;
    this->startLaneIndex = 0;
    this->endLaneIndex = 0;
    this->angle = 0;
    this->angleSpeed = 0;
    this->angleAcc = 0;
    this->tick = 0;
    this->throttle = 0;
    this->speed = 0;
    this->lastSpeed = 0;
    this->acc = 0;
    this->lastAngle = 0;
    this->lastAngleSpeed = 0;
    this->lastAngleAcc = 0;
    this->turboAvailable = 0;
    this->turboFactor = 0;
    this->turbo = 0;
    this->firstTurnAfterTurbo = 0;
    this->lap = 0;
}

void tracer_update(Tracer *this, struct Track *pTrack, int pieceIndex, float inPieceDistance, int startLaneIndex, int endLaneIndex, float angle, int lap, int tick)
{
    // Subtract distances
    float dist = inPieceDistance - this->inPieceDistance;
    
    // Check if we advanced a piece
    if (this->pieceIndex != pieceIndex) {
        dist += track_pieceLength(pTrack, this->pieceIndex, this->startLaneIndex);
        
        // Check if we also completed a switch
        if (this->startLaneIndex != this->endLaneIndex)
            dist += 2; // extra distance for switch
        
        // Check if we also completed a turn
        if (pTrack->pieces[this->pieceIndex].angle != 0 && pTrack->pieces[pieceIndex].angle == 0)
            this->firstTurnAfterTurbo = 0;
    }
    
    float dt = tick - this->tick;
    float speed = dist / dt;
    float acc = (speed - this->speed) / dt;
    float angleSpeed = (angle - this->angle) / dt;
    float angleAcc = (angleSpeed - this->angleSpeed) / dt;
    
    // Update inPieceCount
    if (this->pieceIndex != pieceIndex) {
        // Reset last piece
        pTrack->pieces[this->pieceIndex].alreadySwapped = 0;
        
        this->pieceIndex = pieceIndex;
        this->inPieceCount = 0;
    } else
        this->inPieceCount++;
    
    if (this->turboAvailable > 1)
        this->turboAvailable--;
    
    this->lastSpeed = this->speed;
    this->speed = speed;
    this->acc = acc;
    this->inPieceDistance = inPieceDistance;
    this->startLaneIndex = startLaneIndex;
    this->endLaneIndex = endLaneIndex;
    this->lastAngle = this->angle;
    this->lastAngleSpeed = this->angleSpeed;
    this->lastAngleAcc = this->angleAcc;
    this->angle = angle;
    this->angleSpeed = angleSpeed;
    this->angleAcc = angleAcc;
    this->lap = lap;
    this->tick = tick;
}

void tracer_crash(Tracer *this)
{
    this->angle = 0;
    this->angleSpeed = 0;
    this->angleAcc = 0;
    this->throttle = 0;
    this->speed = 0;
    this->lastSpeed = 0;
    this->acc = 0;
    this->lastAngle = 0;
    this->lastAngleSpeed = 0;
    this->lastAngleAcc = 0;
    this->turbo = 0;
}
