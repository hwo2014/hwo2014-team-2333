//
//  track.c
//  
//
//  Created by Jonathan Noyola on 4/24/14.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "track.h"

#define MAX_ITERATIONS 3

void track_new(Track *this, int numPieces, int numLanes)
{
    this->numPieces = numPieces;
    this->numLanes = numLanes;
    this->pieces = malloc(sizeof(TrackPiece) * numPieces);
    this->lanes = malloc(sizeof(Lane) * numLanes);
    
    this->kEnginePower = -1;
    this->kResistance = -1;
    this->kBreakoutAngle = -1;
    this->kA = -1;
    this->kB = -1;
    this->kD = -1;
    this->kM = -1;
    
    this->isQuals = 0;
    this->laps = 0;
    this->speedForTurboPos = 5.5;
    this->turboTime = -1;
    this->isInitialized = 1;
    this->doneSimulating = 0;
}

void track_delete(Track *this)
{
    free(this->pieces);
    free(this->lanes);
    pthread_join(this->simThread, NULL);
}

void track_initPiece(Track *this, int index)
{
    this->pieces[index].length = 0;
    this->pieces[index].angle = 0;
    this->pieces[index].swap = 0;
    this->pieces[index].shouldSwap = 0;
    this->pieces[index].alreadySwapped = 0;
}

float track_pieceLength(Track *this, int index, int lane)
{
    float angle = this->pieces[index].angle;
    float length = this->pieces[index].length;
    int swap = this->pieces[index].swap;
    if (angle != 0) {
        float offset = this->lanes[lane].offset;
        length = (fabs(angle) / 360) * 2 * M_PI * (length - offset);
    }
    if (swap) {
        length += 2.06;
    }
    return length;
}

int track_normalizePieceIndex(Track *this, int i)
{
    i = i < 0 ? i + this->numPieces : (i >= this->numPieces ? i - this->numPieces : i);
    return i;
}

void track_logTick(Track *this, Tracer *pTracer)
{
    if (this->kEnginePower == -1) {
        if (pTracer->speed > 0) {
            /* a = kT + cv_i, v_i = 0
             * a = kt
             * k = a / T
             */
            this->kEnginePower = pTracer->acc / pTracer->throttle;
            printf("___ENGINE POWER: %f\n", this->kEnginePower);
        }
    } else if (this->kResistance == -1) {
        /* a = kT + cv_i
         * c = (a - kT) / v_i
         */
        this->kResistance = (pTracer->acc - this->kEnginePower * pTracer->throttle) / pTracer->lastSpeed;
        printf("___RESISTANCE: %f\n", this->kResistance);
    } else if (this->kA == -1) {
        if (this->pieces[pTracer->pieceIndex].angle == 0 && pTracer->angle != 0 && pTracer->inPieceCount > 3 && pTracer->startLaneIndex == pTracer->endLaneIndex) {
            float y1 = pTracer->lastAngle;
            float y1d = pTracer->lastAngleSpeed;
            float y1dd = pTracer->lastAngleAcc;
            float y2 = pTracer->angle;
            float y2d = pTracer->angleSpeed;
            float y2dd = pTracer->angleAcc;
            this->kA = (y2 * y1dd / y1 - y2dd) / (y2d - y2 * y1d / y1);
            this->kB = (-y1dd - this->kA * y1d) / y1;
            printf("___kA: %f\n", this->kA);
            printf("___kB: %f\n", this->kB);
        }
    } else if (this->kD == -1) {
        float v1 = pTracer->lastSpeed;
        float v2 = pTracer->speed;
        if (this->pieces[pTracer->pieceIndex].angle != 0 && pTracer->angle != 0 && pTracer->inPieceCount > 3 && v1 != v2 && pTracer->startLaneIndex == pTracer->endLaneIndex) {
            float y1 = pTracer->lastAngle;
            float y1d = pTracer->lastAngleSpeed;
            float y1dd = pTracer->lastAngleAcc;
            float y2 = pTracer->angle;
            float y2d = pTracer->angleSpeed;
            float y2dd = pTracer->angleAcc;
            float r = this->pieces[pTracer->pieceIndex].length - this->lanes[pTracer->startLaneIndex].offset;
            this->kD = (y2dd + this->kA * y2d + this->kB * y2 - (y1dd + this->kA * y1d + this->kB * y1) * r * v2 * v2 / (r * v1 * v1)) / (1 - r * v2 * v2 / (r * v1 * v1));
            this->kM = (y1dd + this->kA * y1d + this->kB * y1 - this->kD) * r / (v1 * v1);
            printf("___kD: %f\n", this->kD);
            printf("___kM: %f\n", this->kM);
        }
    } else if (this->kBreakoutAngle != -1 && this->turboTime != -1 && this->doneSimulating == 0) {
        // Time to simulate!
        this->doneSimulating = -1;
        //pthread_create(&(this->simThread), NULL, track_simulate, this);
    }
}

int track_isDoneLogging(Track *this)
{
    if (this->kEnginePower != -1 && this->kResistance != -1 && this->kA != -1 && this->kB != -1 && this->kD != -1 && this->kM != -1)
        return 1;
    return 0;
}

float track_predictSpeed(Track *this, float speed, float throttle, float dt)
{
    /* a = kT + cv_i
     * (v_f - v_i) / dt = kT + cv_i
     * v_f - v_i = dt(kT + cv_i)
     * v_f = dt(kt + cv_i) + v_i
     */
    return dt * (this->kEnginePower * throttle + this->kResistance * speed) + speed;
}

float track_getDesiredThrottle(Track *this, Tracer *pTracer, float speed, float desiredSpeed, float dt)
{
    if (!track_isDoneLogging(this) || this->kBreakoutAngle == -1)
        return 1.0;
    
    /* a = kT + cv_i
     * (v_f - v_i) / dt = kT + cv_i
     * (v_f - v_i) / dt - cv_i = kT
     * T = ((v_f - v_i) / dt - cv_i) / k
     */
    float T = ((desiredSpeed - speed) / dt - this->kResistance * speed) / this->kEnginePower;
    
    if (pTracer->turbo)
        T /= pTracer->turboFactor;
    
    T = T > 1.0 ? 1.0 : (T < 0.0 ? 0.0 : T);
    return T;
}

float track_getIdealThrottle(Track *this, Tracer *pTracer, float distance)
{
    if (!track_isDoneLogging(this) || this->kBreakoutAngle == -1)
        return 1.0;
    
    if (fabs(pTracer->angle) > this->kBreakoutAngle)
        return 0.0;
    
    float r = this->pieces[pTracer->pieceIndex].length - this->lanes[pTracer->startLaneIndex].offset;
    float kA = this->kA;
    float kB = this->kB;
    float kD = this->kD;
    float kM = this->kM;
    float theta_max = this->kBreakoutAngle;
    float dt = 1;
    
    float dist;
    if (distance == -1) {
        // Find next critical point
        int index = pTracer->pieceIndex;
        dist = -pTracer->inPieceDistance;
        int turned = 0;
        for (int i = index; !turned || this->pieces[i].angle != 0; i = track_normalizePieceIndex(this, i + 1)) {
            dist += track_pieceLength(this, i, pTracer->startLaneIndex);
            if (this->pieces[i].angle != 0) {
                turned = 1;
                index = i;
            }
        }
    } else
        dist = distance;
    //printf("CRIT POINT   index: %d   dist: %f\n", index, dist);
    
    // Find v_max for next critical point
    float v_max;
    if (dist == 0) {
        float theta_1 = fabs(pTracer->angle);
        float theta_0 = fabs(pTracer->lastAngle);
        v_max = sqrt((r / kM) * (theta_max * (1 + kA + kB * dt) - theta_1 * (2 + kA) + theta_0 - kD * dt));
        dist = v_max * dt;
    } else {
        v_max = sqrt((r / kM) * (theta_max * kB - kD)); // assuming theta_2 = theta_1 = theta_0 = theta_max
    }
    if (isnan(v_max))
        return 0.0;
    
    // Find throttle required to get to v_max after driving dist
    float L = 1 + this->kResistance * dt;
    float v = pTracer->speed;
    float T = (v_max - dist * log(L) - v) * L / (this->kEnginePower * dt);
    
    if (pTracer->turbo)
        T /= pTracer->turboFactor;
    
    T = T > 1.0 ? 1.0 : (T < 0.0 ? 0.0 : T);
    return T;
}

int track_isStraight(Track *this, int index, int length)
{
    int isStraight = 1;
    for (int i = 0; i < length; i++) {
        int j = track_normalizePieceIndex(this, index + i);
        if (this->pieces[j].angle != 0) {
            isStraight = 0;
            break;
        }
    }
    return isStraight;
}

float track_getNextAngle(Track *this, Tracer *pTracer)
{
    return track_simNextAngle(this, pTracer->pieceIndex, pTracer->startLaneIndex, pTracer->speed, pTracer->angle, pTracer->angleSpeed);
}

void *track_simulate(void *args) {
    Track *this = (Track *)args;
    
    // Start just before the last turn (so we can hit full throttle from there)
    int foundTurn = 0;
    for (int i = this->numPieces - 1; i > 0; i--) {
        if (this->pieces[i].angle != 0)
            foundTurn = 1;
        else if (foundTurn)
            this->startPieceIndex = i + 1;
    }
    
    int lap = 2;
    int pieceIndex = this->startPieceIndex;
    float inPieceDistance = 0;
    int laneIndex = 0;
    
    this->simTickStart = malloc(sizeof(SimTick));
    this->simTickStart->throttle = 1.0;
    this->simTickStart->speed = 0.0;
    this->simTickStart->angle = 0.0;
    this->simTickStart->angleSpeed = 0.0;
    this->simTickStart->pieceIndex = pieceIndex;
    this->simTickStart->inPieceDistance = 0;
    this->simTickStart->prev = NULL;
    SimTick *currSimTick = this->simTickStart;
    
    int tick = 0;
    while(1) {
        inPieceDistance += currSimTick->speed;
        float length = track_pieceLength(this, pieceIndex, laneIndex);
        if (inPieceDistance > length) {
            inPieceDistance -= length;
            pieceIndex = track_normalizePieceIndex(this, pieceIndex + 1);
            if (pieceIndex == 0) {
                lap++;
                if (lap == this->laps)
                    break;
            }
        }
        
        SimTick *nextSimTick = malloc(sizeof(SimTick));
        nextSimTick->pieceIndex = pieceIndex;
        nextSimTick->inPieceDistance = inPieceDistance;
        currSimTick->next = nextSimTick;
        nextSimTick->prev = currSimTick;
        nextSimTick->next = NULL;
        
        track_setThrottleForSimTick(this, currSimTick, laneIndex);
        printf("===SIMTICK: %d   T: %f   speed: %f   angle: %f\n", tick, currSimTick->throttle, currSimTick->speed, currSimTick->angle);
        tick++;
        currSimTick = nextSimTick;
    }
    
    this->doneSimulating = 1;
    return NULL;
}

void track_setThrottleForSimTick(Track *this, SimTick *simTick, int laneIndex)
{
    float minT = 0.0;
    float maxT = 1.0;
    float T = maxT;
    float angle = track_simAngleForThrottle(this, simTick, T, laneIndex);
    
    if (angle >= this->kBreakoutAngle) {
        // Full throttle doesn't work. Try no throttle.
        T = minT;
        angle = track_simAngleForThrottle(this, simTick, T, laneIndex);
        if (angle < this->kBreakoutAngle) {
            // No throttle works, so ideal throttle is somewhere in the middle.
            for (int i = 0; i < MAX_ITERATIONS; i++) {
                T = (minT + maxT) / 2;
                angle = track_simAngleForThrottle(this, simTick, T, laneIndex);
                if (angle > this->kBreakoutAngle)
                    maxT = T;
                else if (angle < this->kBreakoutAngle)
                    minT = T;
            }
            T = minT;
            angle = track_simAngleForThrottle(this, simTick, T, laneIndex);
        } else {
            for (SimTick *backSimTick = simTick->prev; backSimTick->prev != NULL; backSimTick = backSimTick->prev) {
                // SET THROTTLE FOR TICK (tick - i) TO 0
                backSimTick->throttle = 0.0;
                
                float backSpeed = backSimTick->speed;
                float backAngle = backSimTick->angle;
                float backAngleSpeed = backSimTick->angleSpeed;
                float backAngleAcc = 0;
                
                // Resimulate by stepping forward from this backSimTick
                for (SimTick *backSimTickJ = backSimTick->next; backSimTickJ->next != NULL; backSimTickJ = backSimTickJ->next) {
                    backSpeed = track_predictSpeed(this, backSpeed, 0, 1); // T = 0
                    backAngleAcc = track_simNextAngleAcc(this, backSimTickJ->prev->pieceIndex, laneIndex, backSpeed, backAngle, backAngleSpeed);
                    backAngleSpeed += backAngleAcc;
                    backAngle += backAngleSpeed;
                    
                    backSimTickJ->speed = backSpeed;
                    backSimTickJ->angle = backAngle;
                    backSimTickJ->angleSpeed = backAngleSpeed;
                    
                    backSimTickJ->next->inPieceDistance = backSimTickJ->inPieceDistance + backSpeed;
                    float length = track_pieceLength(this, backSimTickJ->pieceIndex, laneIndex);
                    if (backSimTickJ->next->inPieceDistance > length) {
                        backSimTickJ->next->inPieceDistance -= length;
                        backSimTickJ->next->pieceIndex = track_normalizePieceIndex(this, backSimTickJ->pieceIndex + 1);
                    } else {
                        backSimTickJ->next->pieceIndex = backSimTickJ->pieceIndex;
                    }
                }
                if (backAngle < this->kBreakoutAngle)
                    break;
            }
            
        }
    }
    
    // Set throttle
    simTick->throttle = T;
}

float track_simAngleForThrottle(Track *this, SimTick *simTick, float throttle, int laneIndex)
{
    int lastPieceIndex = simTick->pieceIndex;
    int pieceIndex = simTick->next->pieceIndex;
    simTick->next->speed = track_predictSpeed(this, simTick->speed, throttle, 1);
    float angleAcc2 = track_simNextAngle(this, lastPieceIndex, laneIndex, simTick->speed, simTick->angle, simTick->angleSpeed);
    simTick->next->angleSpeed = simTick->angleSpeed + angleAcc2;
    simTick->next->angle = simTick->angle + simTick->next->angleSpeed;
    float angle3 = track_simNextAngle(this, pieceIndex, laneIndex, simTick->next->speed, simTick->next->angle, simTick->next->angleSpeed);
    return angle3;
}

float track_simNextAngle(Track *this, int pieceIndex, int laneIndex, float speed, float angle, float angleSpeed)
{
    float y2dd = track_simNextAngleAcc(this, pieceIndex, laneIndex, speed, angle, angleSpeed);
    float y2 = angle + angleSpeed + y2dd;
    return y2;
}

float track_simNextAngleAcc(Track *this, int pieceIndex, int laneIndex, float speed, float angle, float angleSpeed)
{
    float F_c;
    if (this->pieces[pieceIndex].angle == 0) {
        F_c = 0;
    } else {
        float v = speed;
        float r = this->pieces[pieceIndex].length - this->lanes[laneIndex].offset;
        F_c = this->kM * v * v / r + this->kD;
        F_c = F_c < 0 ? 0.0 : F_c;
    }
    float y2dd = F_c - this->kA * angleSpeed - this->kB * angle;
    return y2dd;
}
