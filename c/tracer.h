//
//  tracer.h
//  
//
//  Created by Jonathan Noyola on 4/24/14.
//
//

#ifndef _tracer_h
#define _tracer_h

struct Track;

typedef struct Tracer {
    int pieceIndex;
    float inPieceDistance;
    int inPieceCount;
    int startLaneIndex;
    int endLaneIndex;
    float angle;
    float angleSpeed;
    float angleAcc;
    int tick;
    
    float throttle;
    float speed;
    float acc;
    float lastSpeed;
    float lastAngle;
    float lastAngleSpeed;
    float lastAngleAcc;
    int turboAvailable;
    float turboFactor;
    int turbo;
    int firstTurnAfterTurbo;
    int lap;
} Tracer;

void tracer_new(Tracer *this);

void tracer_update(Tracer *this, struct Track *pTrack, int pieceIndex, float inPieceDistance, int startLaneIndex, int endLaneIndex, float angle, int lap, int tick);

void tracer_crash(Tracer *this);

#endif
